# Intro
A portable mouse animation package.
It can be used in several javascript libraries and frameworks.
This project was cloned from package legendary-cursor of Domenicobrz
https://github.com/Domenicobrz/legendary-cursor

## Available Scripts
### Setup
```
npm i animate-cursor
```

### Usage
#### Init library
In the project directory, you can run:
```javascript
import initAnimCursor from 'animate-cursor'
initAnimCursor()
//or
initAnimCursor({
      lineSize: 0.05,
      opacityDecrement: 0.55,
      speedExpFactor: 0.8,
      lineExpFactor: 0.6,
      sparklesCount: 65,
      maxOpacity: 0.99,  // should be a number between [0 ... 1]
      // texture1:         "http://path_to_texture",      // texture displayed on mouse hover
      // texture2:         "http://path_to_texture",      // texture displayed on mouse click
      // texture3:         "http://path_to_texture",      // texture displayed on sparkles
  })
```

#### Take control of animation
```javascript
const legendaryCursor = initAnimCursor()

legendaryCursor.pause()     // temporarily disable animation
legendaryCursor.resume()    // resume animation

```