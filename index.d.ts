export default function initAnimCursor(args: any): {
    init(args: any): void;
    pause(): void;
    resume(): void;
}