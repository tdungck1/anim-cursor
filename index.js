import loadCursor from "./cursor.js"

let LegendaryCursor

function initAnimCursor(options) {
    if (LegendaryCursor) {
        LegendaryCursor.resume()
        return LegendaryCursor
    }
    LegendaryCursor = loadCursor()
    options ? LegendaryCursor.init(options) : LegendaryCursor.init()
    return LegendaryCursor
}



export default initAnimCursor